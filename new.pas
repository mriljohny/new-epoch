unit new;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    pnl1: TPanel;
    edt1: TEdit;
    btn1: TButton;
    lbl1: TLabel;
    procedure btn1Click(Sender: TObject);
  
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  type 
  TField = class(TObject)
  private
    function GetAsString:string; virtual; abstract;
    procedure SetAsString(const Value:string); 
              virtual; abstract;
  public
    property AsString:string read GetAsString
                         write SetAsString;
  end;
  TStringField=class(TField)
  private
    FData : string;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
  end;
  TIntegerField = class(TField)
  private
    FData : integer;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
  end;
  TFloatField=class(TField)
   private
    FData : extended;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
  end;
  TDateTimeField = class(TField)
  private
    FData : TDateTime;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
  end;



var
  Form1: TForm1;
  A: TstringField;
implementation

{$R *.dfm}
function TStringField.GetAsString:string;
begin
  Result := FData;
end;
procedure TStringField.SetAsString(const 
                          Value:string);
begin
  FData := Value;
end;
function TIntegerField.GetAsString:string;
begin
  Result := IntToStr(FData);
end;
procedure TIntegerField.SetAsString(const 
                          Value:string);
begin
  FData := StrToInt(Value);
end;
function TFloatField.GetAsString:string;
begin
  Result := FloatToStrF(FData,ffFixed,7,2);
end;
procedure TFloatField.SetAsString(const 
                      Value:string);
begin
  FData := StrToFloat(Value);
end;
function TDateTimeField.GetAsString:string;
begin
  Result := DateTimeToStr(FData);
end;
procedure TDateTimeField.SetAsString(const Value:string);
begin
  FData := StrToDateTime(Value);
end;
procedure ShowData(AField:TField);
begin
   Form1.Lbl1.Caption := AField.AsString;
end;
procedure TForm1.btn1Click(Sender: TObject);
begin
 A.AsString:= 'ghb';
 ShowData(A);
end;

end.
